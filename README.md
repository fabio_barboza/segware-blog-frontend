# segware-blog-frontend

## Projeto de avaliação da Empresa Segware

Clonar o repositório, entrar dentro da pasta do projeto e executar o comando "npm install" em seguida a aplicação estára pronto para ser iniciada.

Observação: O projeto do Backend deverá estar rodando para que a funcionalidades estajam todas operacionais.

## Esse projeto foi criado utilizando o [Create React App](https://github.com/facebookincubator/create-react-app).

- [Scripts Disponíveirs](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
  - [npm run eject](#npm-run-eject)


## Foi também criado um script para rodar dentro do Docker:

Para criar a imagem:
docker build . -t segware/blog

Para executar a imagem criadar:
docker run -p 80:80 segware/blog


Autor: Fabio Barobza de Oliveira
E-mail: barboza.oliveira@gmail.com

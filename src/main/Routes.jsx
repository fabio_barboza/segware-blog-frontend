import React from 'react'
import { Switch, Route, Redirect } from 'react-router'

import Home from '../components/home/Home'
import Blog from '../components/blog/Blog'

export default props =>
    <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/blog' component={Blog} />
        <Redirect from='*' to='/' />
    </Switch>

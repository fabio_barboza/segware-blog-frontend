import './Footer.css'
import React from 'react'

export default props =>
    <footer className="footer">
        <span>
            <strong>Desenvolvido por Fábio Barboza de Oliveira - barboza.oliveira@gmail.com</strong>
        </span>
    </footer>

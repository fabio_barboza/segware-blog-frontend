import React from 'react'
import Main from '../template/Main'

export default props =>
    <Main icon="home" title="Início"
        subtitle="Projeto de avaliação para empresa Segware">
        <div className='display-4'>Bem Vindo!</div>
        <hr />
        <p className="mb-0">Esse sistema tem como por objetivo demonstrar minhas aptidões com o framework React.</p>
        <br/>
        <p className="mb-0">Não esquecer de iniciar o Backend da Aplicação.</p>
    </Main>

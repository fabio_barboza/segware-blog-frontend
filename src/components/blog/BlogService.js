import axios from "axios";

const baseUrl = 'http://localhost:8080/posts';

export default class BlogService {

    listAll() {
        return axios(baseUrl);
    }

    save(post) {
        const method = post.id ? 'put' : 'post';
        const url = post.id ? `${baseUrl}/${post.id}` : baseUrl;
        return axios[method](url, post);
    }

    vote(post) {
        return axios.put(`${baseUrl}/vote/${post.id}`);
    }

    delete(post) {
        return axios.delete(`${baseUrl}/${post.id}`);
    }
}

import React, {Component} from 'react'

import Moment from 'react-moment';
import 'moment-timezone';
import Main from '../template/Main';

import BlogService from './BlogService';

const headerProps = {
    icon: 'newspaper-o',
    title: 'Blog',
    subtitle: 'Postagens'
};

const initialState = {
    post: {title: '', email: '', text: '', votes: ''},
    list: []
};

let service = new BlogService();

export default class Blog extends Component {

    state = {...initialState};

    componentWillMount() {
        service.listAll().then(resp => {
            this.setState({list: resp.data})
        })
    }

    save() {
        service.save(this.state.post)
            .then(resp => {
                const list = this.getUpdatedList(resp.data);
                this.setState({post: initialState.post, list})
            })
    }

    vote(post) {
        service.vote(post).then(() => {
            service.listAll().then(resp => {
                this.setState({list: resp.data})
            })
        })
    }

    remove(post) {
        service.delete(post).then(() => {
            const list = this.getUpdatedList(post, false);
            this.setState({list})
        })
    }

    clear() {
        this.setState({post: initialState.post})
    }

    getUpdatedList(post, add = true) {
        const list = this.state.list.filter(p => p.id !== post.id);
        if (add) list.unshift(post);
        return list
    }

    updateField(event) {
        const post = {...this.state.post};
        post[event.target.name] = event.target.value;
        this.setState({post})
    }

    load(post) {
        this.setState({post})
    }

    renderForm() {
        return (
            <div className="form">
                <div className="row">
                    <div className="col-12">
                        <div className="form-group">
                            <label>Título</label>
                            <input type="text" className="form-control"
                                   name="title"
                                   value={this.state.post.title}
                                   onChange={e => this.updateField(e)}
                                   placeholder="Digite o título..."/>
                        </div>
                    </div>

                    <div className="col-12">
                        <div className="form-group">
                            <label>Texto</label>
                            <textarea className="form-control"
                                      name="text"
                                      value={this.state.post.text}
                                      onChange={e => this.updateField(e)}
                                      placeholder="Digite o conteúdo..."/>
                        </div>
                    </div>

                    <div className="col-12">
                        <div className="form-group">
                            <label>E-mail</label>
                            <input type="text" className="form-control"
                                   name="email"
                                   value={this.state.post.email}
                                   onChange={e => this.updateField(e)}
                                   placeholder="Digite o e-mail..."/>
                        </div>
                    </div>
                </div>

                <hr/>
                <div className="row">
                    <div className="col-12 d-flex justify-content-end">
                        <button className="btn btn-primary"
                                onClick={e => this.save(e)}>
                            Salvar
                        </button>

                        <button className="btn btn-secondary ml-2"
                                onClick={e => this.clear(e)}>
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        )
    }

    renderTable() {
        return (
            <table className="table mt-4">
                <thead>
                <tr>
                    <th>Ult. Modif.</th>
                    <th>Titulo</th>
                    <th>Conteúdo</th>
                    <th>E-mail</th>
                    <th>Votos</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                {this.renderRows()}
                </tbody>
            </table>
        )
    }

    renderRows() {
        return this.state.list.map(post => {
            return (
                <tr key={post.id}>
                    <td><Moment format="DD/MM/YYYY hh:mm">{post.date}</Moment></td>
                    <td>{post.title}</td>
                    <td>{post.text}</td>
                    <td>{post.email}</td>
                    <td>{post.votes}</td>
                    <td>
                        <div className="flexDiv">
                            <button className="btn btn-info"
                                    onClick={() => this.vote(post)}>
                                <i className="fa fa-check"/>
                            </button>
                            <button className="btn btn-warning ml-2"
                                    onClick={() => this.load(post)}>
                                <i className="fa fa-pencil"/>
                            </button>
                            <button className="btn btn-danger ml-2"
                                    onClick={() => this.remove(post)}>
                                <i className="fa fa-trash"/>
                            </button>
                        </div>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <Main {...headerProps}>
                {this.renderForm()}
                {this.renderTable()}
            </Main>
        )
    }
}
